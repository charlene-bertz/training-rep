'use strict';

// HINT: do not put all require statements at the top of the file
// unless you really need them for all functions

/**
* Description of the function
*
* @return {String} The string 'myFunction'
*/
// var myFunction = function(){
//     return 'myFunction';
// }

/* Exports of the controller */
///**
// * @see {@link module:controllers/Basket~myFunction} */
//exports.MyFunction = myFunction;

/*
 * @module controllers/Basket
 * 
 */

/**
* Get current Basket
*
* @module  controllers/Basket
*/

var guard = require('app_training_controllers/cartridge/scripts/guard');
var ISML = require('dw/template/ISML');

function helloWorld() {
	var basket:Basket = require('dw/order/Basket');
	var basketResult = new dw.system.Pipelet('GetBasket').execute({});
	var basketResult = basketResult.Basket;
	
	ISML.renderTemplate('showBasket.isml', {myBasket:basketResult});
}
exports.Start = guard.ensure(['https', 'get'], helloWorld);

var importBasket = require('dw/order/Basket');

function version2Start() {
	importBasket = require('~/cartridge/scripts/BasketModel').getMyBasket();
	ISML.renderTemplate('showBasket.isml', {myBasket:importBasket});
}
exports.Ver2Start = guard.ensure(['https','get'], start);
