'use strict';

/**
* A hello world controller. This file is in cartridge/controllers folder
* 
* @module controllers/HelloWorld
*  
*/

var guard = require('app_training_controllers/cartridge/scripts/guard');

function helloWorld() {
	var ISML = require('dw/template/ISML');
	var param = request.httpParameterMap.param + 'Vor mir war der Parameter';
	
	ISML.renderTemplate('helloworld.isml', {myMessage:'Hello World! Again', myParameter:param});
}
exports.Start = guard.ensure(['https', 'get'], helloWorld);