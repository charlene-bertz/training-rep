'use strict';

var HTTPClient = require('dw/net/HTTPClient');
var WeatherService = require('~/cartridge/scripts/services/WeatherServiceCall');

/**
 * Get User and fetch location and date to show in widget
 * @param {dw.system.Request} request
 * @returns {Object} Object that holds data for widget
 * 
 */

function getWidgetData(request) {
    var geoLocation = request.getGeolocation();
    var widgetData;

    if (geoLocation) {
        var geoLocationAvailable = geoLocation.isAvailable();
        if (geoLocationAvailable) {
            var userCity = geoLocation.getCity();
            var userCountry = geoLocation.getCountryName();
            var userDate = new Date();
            var userWeather = WeatherService.getCurrentWeather(geoLocation.getLatitude(), geoLocation.getLongitude());
            widgetData = {
                'locationAvailable': geoLocationAvailable,
                'city': userCity,
                'country': userCountry,
                'date': userDate.toLocaleDateString(),
                'weather': 'not available'
            };
            if (userWeather.status != 'SERVICE_UNAVAILABLE') {
                widgetData.weather = userWeather.weather[0].main;
                widgetData.temperatureCelsius = userWeather.main.temp;
                widgetData.temperatureFahrenheit = convertMetricToImperial(userWeather.main.temp);
            }
        }
    }
    return widgetData;
}

function convertMetricToImperial(temperature) {
    var factor = Math.pow(10, 1);
    return Math.round((temperature * (9/5) + 32) * factor) / factor;
}

function isWidgetEnabled() {
    var SitePreferences = dw.system.Site.getCurrent();
    var isWidgetEnabled = SitePreferences.getPreferences().getCustom()['showWeatherWidget'];
    return isWidgetEnabled;
}



function getUserWeather(geoLocation) {
    var httpClient = new HTTPClient();
    var message;
    var requestString = 'api.openweathermap.org/data/2.5/weather?lat=' + geoLocation.getLatitude() + '&lon=' + geoLocation.getLongitude() + '&APPID=1242a07093ce2961580317cff872d296&units=metric';

    httpClient.open('GET', requestString);
    httpClient.send();
    var text = httpClient.text;
    message = JSON.parse(text);
    
    return message;
}

module.exports = {
    getWidgetData: getWidgetData,
    getWidgetStatus: isWidgetEnabled
};