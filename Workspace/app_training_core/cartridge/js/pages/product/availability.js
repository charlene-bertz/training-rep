'use strict';

var ajax = require('../../ajax'),
    Promise = require('promise'),    
    util = require('../../util');

var updateContainer = function (data) {
    var $availabilityMsg = $('#pdpMain .availability .availability-msg');
    var message; // this should be lexically scoped, when `let` is supported (ES6)
    if (!data) {
        $availabilityMsg.html(Resources.ITEM_STATUS_NOTAVAILABLE);
        return;
    }
    $availabilityMsg.empty();
    // Look through levels ... if msg is not empty, then create span el
    if (data.levels.IN_STOCK > 0) {
        if (data.levels.PREORDER === 0 && data.levels.BACKORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
            // Just in stock
            message = Resources.IN_STOCK;
        } else {
            // In stock with conditions ...
            message = data.inStockMsg;
        }
        $availabilityMsg.append('<p class="in-stock-msg">' + message + '</p>');
    }
    if (data.levels.PREORDER > 0) {
        if (data.levels.IN_STOCK === 0 && data.levels.BACKORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
            message = Resources.PREORDER;
        } else {
            message = data.preOrderMsg;
        }
        $availabilityMsg.append('<p class="preorder-msg">' + message + '</p>');
    }
    if (data.levels.BACKORDER > 0) {
        if (data.levels.IN_STOCK === 0 && data.levels.PREORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
            message = Resources.BACKORDER;
        } else {
            message = data.backOrderMsg;
        }
        $availabilityMsg.append('<p class="backorder-msg">' + message + '</p>');
    }
    if (data.inStockDate !== '') {
        $availabilityMsg.append('<p class="in-stock-date-msg">' + String.format(Resources.IN_STOCK_DATE, data.inStockDate) + '</p>');
    }
    if (data.levels.NOT_AVAILABLE > 0) {
        if (data.levels.PREORDER === 0 && data.levels.BACKORDER === 0 && data.levels.IN_STOCK === 0) {
            message = Resources.NOT_AVAILABLE;
        } else {
            message = Resources.REMAIN_NOT_AVAILABLE;
        }
        $availabilityMsg.append('<p class="not-available-msg">' + message + '</p>');
    }
};

var getAvailability = function () {
    ajax.getJson({
        url: util.appendParamsToUrl(Urls.getAvailability, {
            pid: $('#pid').val(),
            Quantity: $(this).val()
        }),
        callback: updateContainer
    });
};

var subscribeToItem = function (e) {
    e.preventDefault();
    e.stopPropagation();
    var $form = $(this).closest('form');
    var $path = $form.context.form.action;
    
    if ($form.find('.email-input').valid()) {
        //Form Data is fetched seperately to ease access in working controller
        var $formData = {
            'email': $form.find('.email-input').val(),
            'product': $form.find('.product-field').val()
        };
        return Promise.resolve($.ajax({
            type: 'POST',
            url: $path,
            data: $formData
        })).then(function (response) {
            if (response.error) {
                throw new Error(response.error);
            } else {
                $form.append(Resources.RESTOCK_SUB_SUCCESS);
                //Button is removed to prevent from subbing multiple times
                $form.find('.subscription-apply').remove();

                //Product and email are set in local storage cause it works on all pages of the site
                localStorage.setItem('restock-info', JSON.stringify($formData));
                //Cookie includes only mail address cause json parsing if faulty in controller
                document.cookie = "restock-mail=" + $formData.email;
                console.log('Transaction successful.');
            }
        });
    } else {
        console.log('Form is not valid');
        return;
    }
};

module.exports = function () {
    $('#pdpMain').on('change', '.pdpForm input[name="Quantity"]', getAvailability);
    $('.restock-notification-form').on('click', '.subscription-apply', subscribeToItem);
};
